const { Schema, model } = require('mongoose');
const moment = require('moment');

const reservaSchema = Schema({
	codigo:{
		type: String
		// required: true,
		// unique:true
	},	
	desde:{
		type: Date	
	},
	hasta:{
		type: Date	
	},
	bicicleta: {
		required: true,
		type: Schema.Types.ObjectId,
		ref: 'Bicicleta'
	},
	usuario: {
		required: true,
		type: Schema.Types.ObjectId,
		ref: 'Usuario'
	},
});

reservaSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});


reservaSchema.methods.diasReserva = function(){
	
	var start = moment(this.desde, "YYYY-MM-DD").startOf('day');
	var end = moment(this.hasta, "YYYY-MM-DD").startOf('day');
	const dias = moment.duration(end.diff(start)).asDays() + 1;	
	return dias;
};


module.exports = model('Reserva', reservaSchema);