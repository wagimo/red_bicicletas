const { Schema, model } = require('mongoose');


const tokenSchema = Schema({
	token:{
		type: String,
		required: true
	},	
	createdOn:{
		type: Date,
		required: true,
		default: Date.now,
		expires:43200
	},	
	usuario: {
		required: true,
		type: Schema.Types.ObjectId,
		ref: 'Usuario'
	},
});

tokenSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});


module.exports = model('Token', tokenSchema);