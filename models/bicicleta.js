
const { Schema, model } = require('mongoose');


const bicicletaSchema = Schema({
	codigo:{
		type: String,
		// required: true,
		//unique:true
	},
	color:{
		type: String		
	},
	modelo:{
		type: String		
	},
	ubicacion:{
		type: [Number],
		index: {
			type: '2dsphere',
			sparse: true
		}		
	},
});

bicicletaSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});

module.exports = model('Bicicleta', bicicletaSchema);


// const Bicicleta = function(id, color, modelo, ubicacion) {
// 	this.id = id;
// 	this.color = color;
// 	this.modelo = modelo;
// 	this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function() {
// 	return `Id:${this.id} | Modelo:${this.modelo} | Color:${this.color} | Ubicación:${this.ubicacion}`;
// }


// Bicicleta.allBike = [];

// Bicicleta.add = function(bici) {
// 	Bicicleta.allBike.push(bici)
// }

// Bicicleta.findById = function(id) {
// 	var bici = Bicicleta.allBike.find(x=> x.id == id );
// 	if(bici)
// 		return bici;
// 	else
// 		 throw new Error('Bici no encontrada!!');
// }

// Bicicleta.remove = function(id) {	
// 	console.log("id->",id);	
// 	var index = Bicicleta.allBike.findIndex(x => x.id == id);	
// 	if (index > -1) {
// 		Bicicleta.allBike.splice(index, 1);
// 	}	
	
// }

// //inicializando 2 bicicletas
// const b1 = new Bicicleta(1,'Negro', 'Scote Scale 940 Mod. 2019', [2.464375, -76.590126]);
// const b2 = new Bicicleta(2,'Naranja', 'Scote Scale 970  Mod. 2021', [2.456648, -76.588910]);

// Bicicleta.add(b1);
// Bicicleta.add(b2);

// module.exports = Bicicleta;