const { Schema, model } = require('mongoose');
const saltRounds = 10;
const bcrypt = require('bcrypt');
const uniqueValidator = require('mongoose-unique-validator');
const configEmail = require('../nodemailer/configEmail'); 
const crypto = require('crypto');
const Token = require('./token');
 

const validarEmail = email =>{
	const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    return re.test(email);
}


const usuarioSchema = Schema({
	codigo:{
		type: String,
		required: true,
		unique:true
	},
	nombre:{
		type: String,
		trim: true,
		required:[true, 'Nombre es obligatorio']	
	},
	email:{
		type: String,
		trim: true,
		required:[true, 'Email es obligatorio'],
		lowercase:true,
		validate:[validarEmail,'Formato incorrecto para el Email'],
		match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/],
		unique:true
	},
	password:{
		type: String,
		required: [true, 'El password es obligatorio!'],	
	},
	passwordResetToken: String,
	passwordResetTokenExpires: Date,
	verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

//librerias externas que se aplican a mongoose para validar 
usuarioSchema.plugin(uniqueValidator, {message: 'El {PATH} ya se encuentra registrado!'});

//funcion que se ejecuta antes de ejecutar el save
usuarioSchema.pre('save', function(next){
	try {
        if(this.isModified('password')){
            this.password = bcrypt.hashSync(this.password, saltRounds);
        }
        next();
    } catch (error) {
        return console.log(error);
    }
});

usuarioSchema.methods.validPassword = function(password) {
    try {        
        return bcrypt.compareSync(password, this.password);
    } catch (error) {
        return console.log(error);
    }
}

usuarioSchema.method('toJSON', function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
});


usuarioSchema.methods.sendEmailActivacion = function(){
	
	
	const token = new Token({token:crypto.randomBytes(16).toString('hex'),usuario: this.id})
	token.save((err) => {
		if(err){
			console.log(err);	
			return;
		} 

		console.log("this.email-->>>", this.email);

		const mailOptions ={
			from:'no-reply@redbicicletas.com',
			to: this.email,
			subject: 'Bienvenido a la Red!',
			html: `
				Hola, ${this.nombre}\n
				Para pertenecer a la Red Mundial de Bicicletas Urbanas \n
				debes primero confirmar tu cuenta. Para hacerlo, da click 
				en el siguiente enlace:\n
				<a href='https://bicicletasurbanas.herokuapp.com/token/confirmation/${token.token}'>Activar cuenta!</a>\n		
			`
		};

		configEmail.sendMail(mailOptions, function(err){
			if(err){
				console.log(err);
				return;
			}
			console.log("Se ha enviado el mensaje de activacion de la cuenta!!");
		})

	});
}


usuarioSchema.methods.resetPassword = function(done){
		
	const token = new Token({token:crypto.randomBytes(16).toString('hex'),usuario: this.id})
	token.save((err) => {
		if(err){
			return done(err);
		} 

		const mailOptions ={
			from:'wagimo@gmail.com',
			to: this.email,
			subject: 'Reinicio de password!',
			html: `
				Hola, ${this.nombre}\n
				Para resetear el password por favor da clic en el siguiente enlace o copialo directamente en el navegador : \n				
				<a href='https://bicicletasurbanas.herokuapp.com/forgotpassword/resetPassword/${token.token}'>Reiniciar Password!</a>\n		
			`
		};

		configEmail.sendMail(mailOptions, function(err){
			if(err){
				console.log("Error de envío -->>>", err);
				return done(err);
			}
			console.log("Se ha enviado el mensaje para el reinicio del password!!");
		});
		done(null);
	});
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log("condition-->>" , condition);
    self.findOne({ 
        $or: [
            {'googleId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if(result) {
                callback(err, result);
            } else {
                console.log('----------- CONDITION -----------');
                console.log(condition);
				let values = {};
				values.codigo = condition.sub,
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = 'admin123';
                console.log('----------- VALUES -----------');
                console.log(values);
                self.create(values, (err, result) => {
                    if(err) {console.log(err);}
                    return callback(err, result);
                });
            }
    });
}


usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log("condition-->>", condition);
    self.findOne({
        $or: [
            {'facebookId': condition.id}, {'email': condition.emails[0].value}
        ]}, (err, result) => {
            if(result) {
                callback(err, result);
            } else {
                console.log('----------- CONDITION -----------');
                console.log(condition);
				let values = {};
				values.codigo = condition.id;
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = 'admin123';
                console.log('----------- VALUES -----------');
                console.log(values);
                self.create(values, (err, result) => {
                    if(err) {console.log(err);}
                    return callback(err, result);
                });
            }
    });
}

module.exports = model('Usuario', usuarioSchema);