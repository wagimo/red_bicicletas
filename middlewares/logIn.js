
exports.loggedIn  = function(req, res, next){
	if(req.user)
	{
		next();
	}
	else{
		console.log("Usuario no autenticado");
		res.redirect('/login');
	}
}