var mymap = L.map('main_map').setView([2.459523, -76.582801], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'	
}).addTo(mymap);




$.ajax({
	dataType: 'json',
	url: 'http://localhost:3000/api/bicicletas',
	success: function(result){
		
			result.bicicletas.forEach(element => {
				L
					.marker(element.ubicacion, {title: element.modelo})
					.addTo(mymap)
					.bindPopup(						
						`
						<b>Nautilius Solutions</b><br>
						<p>Modelo: ${element.modelo}</p>
						<p>Color: ${element.color}</p>
						`
					).openPopup();
			});
	}
});