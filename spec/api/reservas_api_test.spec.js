var request = require('request');
var server = require('../../bin/www');
const mongoose = require('mongoose');

const Reserva = require('../../models/reserva');
const Usuario = require('../../models/usuario');
const Bicicleta = require('../../models/bicicleta');


describe('TESTING API RESERVAS', () => {
	const endPoint = 'http://localhost:3000/api/';

	afterEach(function(done) {
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);           
            Usuario.deleteMany({}, function(err, success) {
				if (err) console.log(err);           
				Bicicleta.deleteMany({}, function(err, success) {
					if (err) console.log(err);           
					done();
				});
			});
		});	
		
	});


	describe('GET RESERVAS /', () => {
		it("status 200",()=>{						
			request
				.get(`${endPoint}reservas`)
				.on('response', function(response) {							 
					expect( response.statusCode ).toBe(200);
				});			
		});
	});	
	
	describe('POST RESERVAS /Create', () => {
		it("status 200",(done)=>{

			
			const user = new Usuario({codigo:'usr_01', nombre:'Jose Luis Fernandez'});
			Usuario.create(user, function(err, success){
				if (err) console.log(err)
				done();
			});
			
			
			var bici = new Bicicleta({ codigo: "bici_001", color: "azul", modelo: "montaña", ubicacion: [-31.352241, -64.184132] });
            Bicicleta.create(bici, function(err, success){
				if (err) console.log(err)
                done();
			});
           

			var headers = {'content-type': 'application/json'};

			const reserva = `{"codigo":"rsv_test_008","desde":"2020/12/22", "hasta":"2020/12/25", "usuario":"${user.id}", "bicicleta":"${bici.id}"}`;
			console.log("reserva JSO->>", reserva);
			request
				.post(
						{
							headers: headers,
							url : `${endPoint}reservas/create`,
							body: reserva
						},
						function(error, response,body) {	
							if(error) console.log("Error al crear la reserva", error);								
							expect( response.statusCode ).toBe(200);							
							done();					
				});			
		});
	});	

	// describe('PUT RESERVAS /Update', () => {
	// 	it("status 200",(done)=>{
	// 		var headers = {'content-type': 'application/json'};			
			
			
	// 		const user = new Usuario({codigo:'usr_01', nombre:'Jose Luis Fernandez'});
	// 		Usuario.create(user, function(err, success){
	// 			if (err) console.log(err)
	// 			done();
	// 		});
			
			
	// 		var bici = new Bicicleta({ codigo: "bici_001", color: "azul", modelo: "montaña", ubicacion: [-31.352241, -64.184132] });
    //         Bicicleta.create(bici, function(err, success){
	// 			if (err) console.log(err)
    //             done();
	// 		});
		   			
	// 		const reserva = `{"codigo":"rsv_test_001","desde":"2020/12/22", "hasta":"2020/12/25", "usuario":"${user.id}", "bicicleta":"${bici.id}"}`;
	// 		const reservaUpdate = `{"codigo":"rsv_test_001","desde":"2020/12/25", "hasta":"2020/12/28", "usuario":"${user.id}", "bicicleta":"${bici.id}"}`;

	// 		request
	// 			.post(
	// 					{
	// 						headers: headers,
	// 						url : `${endPoint}reservas/create`,
	// 						body: reserva
	// 					},
	// 					function(error, response,body) {	
	// 						if(error) console.log("Error al crear la reserva", error);	
							
	// 						expect( response.statusCode ).toBe(200);


	// 						request
	// 							.put(
	// 									{
	// 										headers: headers,
	// 										url : `${endPoint}reservas/update`,
	// 										body: reservaUpdate
	// 									},
	// 									function(error, response,body) {	
	// 										if(error) console.log("Error al actualizar los datos de la reserva", error);						
	// 										expect( response.statusCode ).toBe(200);							
	// 										done();					
	// 							});	

	// 			});						
	// 	});
	// });

	// describe('DELETE RESERVAS /delete', () => {
	// 	it("status 204",(done)=>{
	// 		var headers = {'content-type': 'application/json'};
			
	// 		const reserva = '{ "id":"5fe2613c7eb8420ffeee75bc" }';

	// 		request
	// 			.delete(
	// 					{
	// 						headers: headers,
	// 						url : `${endPoint}reservas/delete`,
	// 						body: reserva
	// 					},
	// 					function(error, response,body) {
	// 						if(error) console.log("Error al eliminar la reserva", error);	
	// 						expect( response.statusCode ).toBe(204);							
	// 						done();
	// 			});
	// 		});
	// });


});

