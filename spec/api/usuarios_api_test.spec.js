const Usuario  = require('../../models/usuario');
var request = require('request');
var server = require('../../bin/www');
const mongoose = require('mongoose');


describe('Usuario API', () => {
	const endPoint = 'http://localhost:3000/api/';

	afterEach(function(done) {
        Usuario.deleteMany({}, function(err, success) {
            if (err) console.log(err);           
            done();
        });
	});
	
	describe('GET USUARIOS /', () => {
		it("status 200",()=>{						
			request
				.get(`${endPoint}usuarios`)
				.on('response', function(response) {							 
					expect( response.statusCode ).toBe(200);
				});			
		});
	});	
	
	describe('POST USUARIOS /Create', () => {
		it("status 200",(done)=>{
			var headers = {'content-type': 'application/json'};

			const user = '{ "codigo":"usr_test_15","nombre":"Carlos Martinez", "email":"carlos@gmail.com", "password":"carlos123"}';						
			request
				.post(
						{
							headers: headers,
							url : `${endPoint}usuarios/create`,
							body: user
						},
						function(error, response,body) {							
							expect( response.statusCode ).toBe(200);							
							done();					
				});			
		});
	});	

	describe('PUT USUARIOS /Update', () => {
		it("status 200",(done)=>{
			var headers = {'content-type': 'application/json'};

			const userUpdate = '{ "codigo":"usr_test_155","nombre":"Carlos Martinez Tovar"}';
			const userCreated ='{ "codigo":"usr_test_155","nombre":"Carlos Martinez","email":"carlos@gmail.com", "password":"carlos123"}';		

			request
				.post(
						{
							headers: headers,
							url : `${endPoint}usuarios/create`,
							body: userCreated
						},
						function(error, response,body) {							
							expect( response.statusCode ).toBe(200);							
							
							request
								.put(
										{
											headers: headers,
											url : `${endPoint}usuarios/update`,
											body: userUpdate
										},
										function(error, response,body) {	
											if(error) console.log("Error al actualizar los datos del usuario", error);						
											expect( response.statusCode ).toBe(200);							
											done();					
								});								
						});	
				});
	});

	describe('DELETE USUARIOS /delete', () => {
		it("status 204",(done)=>{
			var headers = {'content-type': 'application/json'};	

			const userDeleted = '{ "codigo":"usr_test_155" }';
			const userCreated ='{ "codigo":"usr_test_155","nombre":"Carlos Martinez","email":"carlos@gmail.com", "password":"carlos123"}';							
			request
				.post(
						{
							headers: headers,
							url : `${endPoint}usuarios/create`,
							body: userCreated
						},
						function(error, response,body) {							
							expect( response.statusCode ).toBe(200);							
							
							request
								.delete(
										{
											headers: headers,
											url : `${endPoint}usuarios/delete`,
											body: userDeleted
										},
										function(error, response,body) {
											if(error) console.log("Error al eliminar el usuario", error);	
											expect( response.statusCode ).toBe(200);							
											done();
								});
						});					
			});
		});
 });