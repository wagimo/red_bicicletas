const Bicicleta  = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
const mongoose = require('mongoose');


describe('Bicicleta API', () => {
	const endPoint = 'http://localhost:3000/api/';


	afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);           
            done();
        });
    });


	describe('GET BICILETAS /', () => {
		it("status 200",()=>{
			
			request
				.get(`${endPoint}bicicletas`)
				.on('response', function(response) {					 
					expect( response.statusCode ).toBe(200);
				});			
		});
	});	
	
	describe('GET BICILETAS /Create', () => {
		it("status 200",(donde)=>{
			var headers = {'content-type': 'application/json'};
			const b1 = '{ "codigo":"1","color":"Negro","modelo":"Scote Scale 940 Mod. 2019","latitud":"2.464375","longitud":"-76.590126"}';						
			request
				.post(
						{
							headers: headers,
							url : `${endPoint}bicicletas/create`,
							body: b1
						},
						function(error, response,body) {
							if(error) console.log("Error al crear la bici", error);
							expect( response.statusCode ).toBe(200);							
							donde();
				});			
		});
	});	

	describe('GET BICILETAS /Update', () => {
		it("status 200",(donde)=>{
			var headers = {'content-type': 'application/json'};

			const biciCreate = '{ "codigo":"1","color":"Negro","modelo":"Scote Scale 940 Mod. 2019","latitud":"2.464375","longitud":"-76.590126"}';						
			const biciUpdate = '{ "codigo":"1","color":"Negro Mate","modelo":"Scote Scale 940 Mod. 2020","latitud":"2.464375","longitud":"-76.590126"}';

			request
				.post(
						{
							headers: headers,
							url : `${endPoint}bicicletas/create`,
							body: biciCreate
						},
						function(error, response,body) {
							if(error) console.log("Error al crear la bici", error);
							expect( response.statusCode ).toBe(200);
								request
									.put(
											{
												headers: headers,
												url : `${endPoint}bicicletas/update`,
												body: biciUpdate
											},
											function(error, response,body) {
												if(error) console.log("Error al actualizar la bici", error);
												expect( response.statusCode ).toBe(200);											
												donde();
									});	
						});	
					
				});
	});	

	describe('GET BICILETAS /delete', () => {
		it("status 204",(done)=>{
			var headers = {'content-type': 'application/json'};
			
			const biciCreate = '{ "codigo":"1","color":"Negro","modelo":"Scote Scale 940 Mod. 2019","latitud":"2.464375","longitud":"-76.590126"}';						
			
			const bDelete = '{ "codigo":"1" }';

			request.post(
						{
							headers: headers,
							url : `${endPoint}bicicletas/create`,
							body: biciCreate
						},
						function(error, response,body) {
							if(error) console.log("Error al crear la bici", error);
							expect( response.statusCode ).toBe(200);	
							
							request.delete(
								{
									headers: headers,
									url : `${endPoint}bicicletas/delete`,
									body: bDelete
								},
								function(error, response,body) {
									expect( response.statusCode ).toBe(200);									
									done();
								}
							);	
						}
				);						
			});
	});	

});

