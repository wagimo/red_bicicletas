
const mongoose = require('mongoose');
const Usuario  = require('../../models/usuario');
var server = require('../../bin/www');

describe('TESTING MODELO DE USUARIOS', () => {
	
	beforeEach((done)=> {
		var mongoDb = 'mongodb://localhost/red_bicicletasTest';
		const db = mongoose.createConnection(mongoDb, {useNewUrlParser:true})		
		db.on('error',console.error.bind(console,'connection error'));
		db.once('open', ()=>{
			console.log('Connection DB Test OK');
			done();
		});		
	} );

	afterEach((done)=> {
		Usuario.deleteMany({},(err,success)=>{
			if(err) console.log(err);
			done();
		});
	} );


	describe('Usuario.AllUsers', () => {
		it('La colección debe retornar un acantidad diferente de cero!', (done)=>{
			
			const user = new Usuario({codigo:'usr_1',nombre:'Walter Molano', email:'wagimo@gmail.com',password:'admin123'});

			user.save( function(err, newUser){				
				Usuario.find({},function(err,usersList){
					if(err){
						console.log("Error al obtener la lista de usuarios",err);
						return;
					} 
					expect( usersList.length  ).toBe(1);
					expect( usersList[0].codigo  ).toBe(newUser.codigo);
					expect( usersList[0].email  ).toBe(newUser.email);					
					done()
				 });
			});
			
		});
	});


	describe('Usuario.Add', () => {
		it('Agrega a la BD un usuario!', (done)=>{

				
		const user = new Usuario({codigo:'usr_1',nombre:'Walter Molano', email:'wagimo@gmail.com',password:'admin123'});
		
		user.save(function(err,newUser){
			if(err) {
				console.log("Error al guardar el usuario en la BD!", err)
				return;
			};

			Usuario.findOne({email:`${newUser.email}`},function(err,userFound){
				
				if(err) {
					console.log("Error al obtener la lista de usuarios",err);
					return;
				}
				expect(userFound.codigo).toEqual(newUser.codigo)
				expect(userFound.nombre).toEqual(newUser.nombre)
				expect(userFound.email).toEqual(newUser.email)
				done()
				});
			});
		});
	});

	describe('Usuario.Modify', () => {
		it('Actualiza la información de un usuario', (done)=>{

			const user = new Usuario({codigo:'usr_1',nombre:'Walter Molano', email:'wagimo@gmail.com',password:'admin123'});

			user.save((err, newUser)=>{

				if(err){
					console.log("Error al insertar el usuario en la BD", err);
					return;
				} 

				Usuario.findOne({email:`${newUser.email}`}, (err1, userFound)=>{
					if(err1){
						console.log("Error buscar el usuario en BD", err1);
						return;
					} 
					
					userFound.codigo = "11";
					userFound.nombre = "Juan Antonio Fernandez";
					const id = userFound.id;
					
					Usuario.findByIdAndUpdate(id, userFound ,(err2) =>{											
						if(err2) {
							console.log("Error al actualizar el usuario en BD", err2);						
							return;
						}
						Usuario.find({},function(err3,usuariosList){

							if(err3) console.log("Error al obtener la lista de usuarios",err);
							expect( usuariosList.length  ).toBe(1);
							expect(usuariosList[0].codigo).toEqual(userFound.codigo)
							expect(usuariosList[0].nombre).toEqual(userFound.nombre)							
							done()
						 });					
					});
				});
			});
		});
	});

	
	describe('Usuario.Delete', () => {
		it('Elimina el usuario por codigo', done=>{

			const user1 = new Usuario({codigo:'usr_1',nombre:'Walter Molano', email:'wagimo@gmail.com',password:'admin123'});
			const user2 = new Usuario({codigo:'usr_2',nombre:'Juan Molano', email:'juan@gmail.com',password:'admin12345'});
	

			user1.save(function(err,u1){
				if(err) {
					console.log(err);
					return;
				}
				
				user2.save(function(err1,u2){
					if(err1){
						console.log(err1);						
						return;
					} 

					Usuario.deleteOne({codigo:u1.codigo},function(err3){

						if(err3){
							console.log(err3);						
							return;
						}

						Usuario.find({},function(err4,usuariosList){
							if(err4){
								console.log(err4);
								return;
							} 
							expect( usuariosList.length  ).toBe(1);
							expect( usuariosList[0].codigo).toEqual(user2.codigo);
							expect( usuariosList[0].nombre).toEqual(user2.nombre);
							done()
							});							
						});						
					});
				});				 
		});
	});


	/**FIN DE ARCHIVO */
});
