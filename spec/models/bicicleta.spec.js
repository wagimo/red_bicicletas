
const mongoose = require('mongoose');
const Bicicleta  = require('../../models/bicicleta');
var server = require('../../bin/www');


describe('Testing API Bicicletas AND monggose', () => {
	const endPoint = 'http://localhost:3000/api/';

	beforeEach((done)=> {			

		var mongoDb = 'mongodb://localhost/red_bicicletasTest';
		const db = mongoose.createConnection(mongoDb, {useNewUrlParser:true})		
		db.on('error',console.error.bind(console,'connection error'));
		db.once('open', ()=>{
			console.log('Connection DB Test OK');
			done();
		});		
	} );

	afterEach((done)=> {
		Bicicleta.deleteMany({},(err,success)=>{
			if(err) console.log(err);
			done();
		});
	} );


	describe('Bicicletas.NuevaInstancia', () => {
		it('Crea un nuevo objeto Bicileta para insertarlo posteriormente en base de datos', ()=>{

			const b1 = JSON.parse( '{ "codigo":"1","color":"Negro","modelo":"Scote Scale 940 Mod. 2019","latitud":"2.464375","longitud":"-76.590126"}');			
			const bici = new Bicicleta( b1 );
			bici.ubicacion = [b1.latitud, b1.longitud];
			expect( bici.codigo ).toBe('1');
			expect( bici.color ).toBe('Negro');
			expect( bici.modelo ).toBe('Scote Scale 940 Mod. 2019');
			expect( bici.ubicacion[0] ).toEqual(2.464375);
			expect( bici.ubicacion[1] ).toEqual(-76.590126);
		});
	});

	describe('Bicicletas.AllBicicletas', () => {
		it('Inicialmente la lista debe estar vacia', (done)=>{

			 Bicicleta.find({},function(err,bicis){
				if(err) console.log(err);
				expect( bicis.length  ).toBe(0);
				done()
			 });
		});
	});

	describe('Bicicletas.Add', () => {
		it('Agrega a la BD una Bicicleta!', (done)=>{

			const b1 = JSON.parse( '{ "codigo":"1","color":"Negro","modelo":"Scote Scale 940 Mod. 2019","latitud":"2.464375","longitud":"-76.590126"}');			
			const bici = new Bicicleta( b1 );
			bici.ubicacion = [b1.latitud, b1.longitud];

			bici.save(function(err,bici){
				if(err) console.log(err);
				Bicicleta.find({},function(err,bicis){
					expect( bicis.length  ).toBe(1);
					expect(bicis[0].codigo).toEqual(bici.codigo)
					done()
				 });
			 });
		});
	});


	describe('Bicicletas.FindBycode', () => {
		it('Busca una sola bicicleta por codigo!', (done)=>{


			Bicicleta.find({},function(err,bicis){
				if(err) console.log(err);

				expect( bicis.length  ).toBe(0);				

				const b1 = JSON.parse( '{ "codigo":"1","color":"Negro","modelo":"Scott Scale 940 Mod. 2019","latitud":"2.464375","longitud":"-76.590126"}');			
				const b2 = JSON.parse( '{ "codigo":"2","color":"Azul","modelo":"Scott Scale 970 Mod. 2020","latitud":"2.464345","longitud":"-76.590136"}');			
				const bici1 = new Bicicleta( b1 );
				bici1.ubicacion = [b1.latitud, b1.longitud];
				const bici2 = new Bicicleta( b2 );
				bici2.ubicacion = [b2.latitud, b2.longitud];

				bici1.save(function(err,bike1){
					if(err) console.log(err);
					
					bici2.save(function(err1,bike2){
						if(err1) console.log(err1);						

						Bicicleta.findOne({"codigo":"1"},function(err,biciEncontrada){
							expect( biciEncontrada.codigo  ).toBe(bike1.codigo);
							expect(biciEncontrada.color).toEqual(bike1.color)
							expect(biciEncontrada.modelo).toEqual(bike1.modelo)
							done()
						 });						
					 });
				 });
			 });			
		});
	});

	describe('Bicicletas.RemoveBycode', () => {
		it('Elimina una sola bicicleta por codigo!', (done)=>{


			Bicicleta.find({},function(err,bicis){
				if(err) console.log(err);
				
				expect( bicis.length  ).toBe(0);				

				const b1 = JSON.parse( '{ "codigo":"1","color":"Negro","modelo":"Scott Scale 940 Mod. 2019","latitud":"2.464375","longitud":"-76.590126"}');			
				const b2 = JSON.parse( '{ "codigo":"2","color":"Azul","modelo":"Scott Scale 970 Mod. 2020","latitud":"2.464345","longitud":"-76.590136"}');			
				const bici1 = new Bicicleta( b1 );
				bici1.ubicacion = [b1.latitud, b1.longitud];
				const bici2 = new Bicicleta( b2 );
				bici2.ubicacion = [b2.latitud, b2.longitud];

				bici1.save(function(err,bike1){
					if(err) console.log(err);
					
					bici2.save(function(err1,bike2){
						if(err1) console.log(err1);						

						Bicicleta.deleteOne({"codigo":"1"},function(err,biciEncontrada){

							Bicicleta.find({},function(err,bicis){
								if(err) console.log(err);
								expect( bicis.length  ).toBe(1);
								done()
							 });							
						 });						
					 });
				 });
			 });			
		});
	});
});


