const mongoose = require('mongoose');
var server = require('../../bin/www');
const Usuario = require('../../models/usuario');
const Reserva = require('../../models/reserva');
const Bicicleta = require('../../models/bicicleta');
const moment = require('moment');

describe('TESTING RESERVAS', () => {
	beforeEach((done)=> {	
		var mongoDb = 'mongodb://localhost/red_bicicletas';
		const db = mongoose.createConnection(mongoDb, {useNewUrlParser:true})		
		db.on('error',console.error.bind(console,'connection error'));
		db.once('open', ()=>{
			console.log('Connection DB Test OK');
			done();
		});		
	} );


	afterEach((done)=> {
		Reserva.deleteMany({},(err,success)=>{
			if(err) console.error("Error al aliminar las reservas", err);
			Usuario.deleteMany({},(err, success)=>{
				if(err) console.error("Error al aliminar los usuarios", err);
				Bicicleta.deleteMany({},(err,success)=>{
					if(err) console.error("Error al aliminar las bicicletas", err);
					done();
				});
			});						
		});
	} );


	describe('Reservas.Add', () => {
		it('Adicionando una reserva a la colección en la BD', done=>{

			var manana =  moment( new Date() ).add(1, 'days');
			const usuario = new Usuario({codigo:'usr_1', nombre:'Walter Molano',email:'pepe@gmail.com',password:'pepe123'});
			usuario.save(function(err, newUser){

				const bici = new Bicicleta({ codigo:"b_1",color:"Azul",modelo:"Scott Scale 970 Mod. 2020",ubicacion:[2.464345,-76.590136]} );
				bici.save(function(err, newBici){

					const reserva = new Reserva({codigo:'rsv_1',desde:new Date(), hasta: manana, bicicleta:bici.id,usuario:usuario.id });
					
					reserva.save(function(err, newReserva){
						if(err) console.log("Error al guardar la reserva", err);

						Reserva.find()
									.populate('usuario')
									.populate('bicicleta')
									.exec(function(err, response){ 
										if(err) console.log(err);																	
										expect(response.length).toBeGreaterThan(0);
										expect(response[1].bicicleta.codigo).toEqual(newBici.codigo);	
										expect(response[1].usuario.codigo).toEqual(newUser.codigo);			
										expect(response[1].codigo).toEqual(newReserva.codigo);	
										done();
									});
					});
				});

			});

		});
	});
})
