var passport = require('passport'),
	localStrategy = require('passport-local').Strategy;
var Usuario = require('../models/usuario');
const GoogleStrategy = require('passport-google-oauth2').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new  localStrategy(
	function(userName, password, done){
		Usuario.findOne({email:userName}, (err, usr)=>{
			if(err) return done(err);
			if(!usr) return  done(null, false,{message: 'Nombre de usuario incorrecto!'})
			if(!usr.validPassword(password)) return  done(null, false, {message:'Password incorrecto!'});
			if(!usr.verificado) return  done(null, false, {message:'La cuenta no ha sido activada. Por favor, revise su correo y active la cuenta por medio del Email enviado por la plataforma.!'});
			return done(null, usr);
		});
	}
));

passport.use(new GoogleStrategy({
	clientID: process.env.GOOGLE_CLIENT_ID,
	clientSecret: process.env.GOOGLE_CLIENT_SECRET,
	callbackURL: process.env.HOST + "/auth/google/callback"
},
 function(accesstoken,refreshToken, profile, cb){
	console.log("profile -->", profile);
	Usuario.findOneOrCreateByGoogle(profile, function(err, user){
		return cb(err, user);
	});
 }
));


passport.use(new FacebookTokenStrategy({
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET
    }, function(accessToken, refreshToken, profile, done) {
        try {
            Usuario.findOneOrCreateByFacebook(profile, function(err, usuario) {
                if (err) { 
                    console.log('err: '+ err); 
                }
                return done(err, usuario)
            });
        } catch (error) {
            console.log(error);
            return done(err, null);
        }
    }
));







passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    Usuario.findById(id, function(err, usuario) {
        done(err, usuario);
    });
});

module.exports = passport;