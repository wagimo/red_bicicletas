require('newrelic');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('dotenv').config();
const cors = require('cors');
const { dbConnection } = require('./database/config');

const passport = require('./config/passport');
const session = require('express-session');
const mongoDbStore = require('connect-mongodb-session')(session);
const {loggedIn} = require('./middlewares/logIn');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const { assert } = require('console');

let store;
if( process.env.ENVIRONMENT == 'development')
{
  store = new session.MemoryStore;
}
else{
  store = new mongoDbStore({
    uri: process.env.DBCONNECTION,
    collection: 'sessions'
  });
  store.on('error', function(err){
    assert.ifError(err);
    assert.ok(false);
  });
}
var app = express();

//esquema de sessiones
app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 100 },
  store: store,
  saveUninitialized: true,
  resave:'true',
  secret:'red_bicicletas_urbanas_&%$"#)==?_123456789?=/&!!!"..'
}));

// INICIANDO CONEXIÓN A LA BD DE MONGO
dbConnection();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//CONFIGURANDO CORS EN EL MIDDELWEARE
// app.use(cors);
app.use(logger('dev'));
//lectura y parseo del body
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);



/** ******************/
app.use('/bicicletas',loggedIn , require('./routes/bicicletas'));
app.use('/usuarios',loggedIn , usersRouter);
app.use('/token', require('./routes/tokens'));
app.use('/login', require('./routes/auth'));
app.use('/forgotpassword', require('./routes/forgotPassword'));


/**APIS */
app.use('/api/bicicletas', require('./routes/api/bicicletas'));
app.use('/api/usuarios', require('./routes/api/usuarios'));
app.use('/api/reservas', require('./routes/api/reservas'));
app.use('/api/login', require('./routes/api/auth'));
app.use('/politica', function(req, res){
  res.sendFile('/public/policy-privacy.html');
});
app.use('/googlea4d1ed9ae9ad2d68', function(req, res){
  res.sendFile('/public/googlea4d1ed9ae9ad2d68.html');
});

app.get('/auth/google',
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/userinfo.email',
    'https://www.googleapis.com/auth/userinfo.profile'
  ]
  })
);

app.get('/auth/google/callback',
    passport.authenticate('google', {
        successRedirect: '/',
        failureRedirect: '/error'
}));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  // res.locals.message = err.message;
  // res.locals.error = process.env.ENVIRONMENT === 'development' ? err : {};

  // // render the error page
  // res.status(err.status || 500);
  // res.render('error');
});

module.exports = app;
