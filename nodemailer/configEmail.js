const nodemailer = require( 'nodemailer' );

const sgTransport = require('nodemailer-sendgrid-transport');



const configProduction = ()=>{
	const options = {
		auth:{
			api_key:process.env.SENDGRIDAPI
		}
	}
	return sgTransport(options);
};

const configLocal = () =>{
	return {
		host: 'smtp.ethereal.email',
		port: 587,
		auth: {
			user: process.env.etheral_user,
			pass: process.env.etheral_pwd	
		}
	}
}

let mailConfig = configProduction();

switch(process.env.ENVIRONMENT)
{
	case 'production':
		mailConfig = configProduction();
		break;
	case 'staging':
		mailConfig = configProduction();
	break;
	case 'development':
		mailConfig = configLocal();
	break;	

}


module.exports = nodemailer.createTransport(mailConfig);