const mongoose = require('mongoose');

const dbConnection = async() => {
    try {       
		
		await mongoose.connect(process.env.DBCONNECTION,{useNewUrlParser:true});
		var db = mongoose.connection;
		db.on('error', console.error.bind(console,'Error'));
        console.log("bd online");
    } catch (err) {
        console.error(err);
        throw new Error("Error al iniciar la BD!");
    }
}


module.exports = {
    dbConnection
}