var  loginController = require('../../controllers/api/auth');
const { validarJWT } = require('../../middlewares/validarToken');

var express = require('express');
const passport = require('../../config/passport');

var router = express.Router();

router.post('/', loginController.loginusuario);
router.post('/forgotPassword', loginController.forgotPassword);
router.get('/renew',
	validarJWT,
	loginController.renewToken);
router.post('/facebook_token', passport.authenticate('facebook-token'), loginController.authFacebookToken);

module.exports = router;