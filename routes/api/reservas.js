var  reservaController = require('../../controllers/api/reservaApi');
const { validarJWT } = require('../../middlewares/validarToken');

var express = require('express');

var router = express.Router();

router.get('/', 
	validarJWT,
	reservaController.getReservas);
router.post('/create',
	validarJWT, 
	reservaController.crearReserva);
router.put('/update',
	validarJWT,
	reservaController.updateReserva);
router.delete('/delete', 
	validarJWT,
	reservaController.eliminarReserva);

module.exports = router;