var  usuarioController = require('../../controllers/api/usuarioApi');
const { validarJWT } = require('../../middlewares/validarToken');

var express = require('express');

var router = express.Router();

router.get('/', 
	validarJWT,
	usuarioController.getUsuarios);
router.post('/create', 
	validarJWT,
	usuarioController.crearUsuario);
router.put('/update', 
	validarJWT,
	usuarioController.updateUsuario);
router.delete('/delete', 
	validarJWT,
	usuarioController.eliminarUsuario);

module.exports = router;