var  biciController = require('../../controllers/api/bicicletaApi');
const { validarJWT } = require('../../middlewares/validarToken');

var express = require('express');

var router = express.Router();

router.get('/',
	validarJWT,
	biciController.getBicicletas);
router.post('/create', 
	validarJWT,
	biciController.crearBicicleta);
router.put('/update', 
	validarJWT,
	biciController.updateBicicleta);
router.delete('/delete', 
	validarJWT,
	biciController.eliminarBicicleta);

module.exports = router;