var express = require('express');
var router = express.Router();

const { 
	forgotPassword,
	forgotPasswordpost,
	forgotPasswordResult,
	resetPasswordGet,
	resetPasswordPost
} = require('../controllers/forgotPassword' );


router.get('/', forgotPassword);
router.post('/', forgotPasswordpost);
router.get('/result', forgotPasswordResult);
router.get('/resetPassword/:token', resetPasswordGet);
router.post('/resetPassword', resetPasswordPost);

module.exports = router;