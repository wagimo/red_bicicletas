var express = require('express');
var router = express.Router();

const { 
	login,
	loginPost,
	logout
} = require('../controllers/auth' );


router.get('/', login);
router.get('/logout', logout);
router.post('/', loginPost );


module.exports = router;