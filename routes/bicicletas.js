var express = require('express');
var router = express.Router();


const { 
	bicicleta_list,
	bicicleta_create_get,
	bicicleta_create_post,
	bicicleta_delete_post,
	bicicleta_update_get,
	bicicleta_update_post,
	bicicleta_preview_get
	
} = require('../controllers/bicicleta' );

router.get('/', bicicleta_list);
router.get('/create', bicicleta_create_get);
router.post('/create', bicicleta_create_post);
router.post('/:id/delete', bicicleta_delete_post);
router.get('/:id/update', bicicleta_update_get);
router.post('/:id/update', bicicleta_update_post);
router.get('/:id/preview', bicicleta_preview_get);

module.exports = router;
