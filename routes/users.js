var express = require('express');
var router = express.Router();

const { 
	usuarios_list,
	usuarios_create_get,
	usuarios_create_post,	
	usuarios_update_get,
	usuarios_update_post,
  	usuario_delete_post,
  	usuario_preview_get
	
} = require('../controllers/usuario' );

/* GET users listing. */
router.get('/', usuarios_list);
router.get('/create', usuarios_create_get);
router.post('/create', usuarios_create_post);
router.get('/:id/update', usuarios_update_get);
router.post('/:id/update', usuarios_update_post);
router.post('/:id/delete', usuario_delete_post);
router.get('/:id/preview', usuario_preview_get);

module.exports = router;
