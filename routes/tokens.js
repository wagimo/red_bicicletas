var express = require('express');
var router = express.Router();

const { 
	confirmarToken
} = require('../controllers/token' );

router.get('/confirmation/:token', confirmarToken);

module.exports = router;