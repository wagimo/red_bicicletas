const { response } = require("express");
var Usuario = require('../../models/usuario');

const getUsuarios = async(req, resp = response) => {
    try {

        const usuarios = await Usuario.find();

        return resp.status(200).json({
            ok: true,
            usuarios
        });
    } catch (error) {

        return resp.status(500).json({
            ok: true,
            msg: 'Error al retornar la lista de usuarios!'
        });
    }
};


const crearUsuario = async(req, resp = response) => {
    
    const usuario = new Usuario({       
        ...req.body
    });

	//const {codigo} = req.body;
    try {

		
		// const result  = await  Usuario.findOne({codigo});
		
		// if (result) {
        //     return resp.status(400)
        //         .json({
        //             ok: false,
        //             msg: 'El código ingresado ya se encuentra registrado!'
        //         });
        // }


        const usuarioBd = await usuario.save();
        return resp.status(200).json({
            ok: true,
            msg: usuarioBd
        });

    } catch (error) {
        return resp.status(500).json({
            ok: true,
            msg: 'Error inesperado.'
        });
    }
};

const eliminarUsuario = async(req, resp = response) => {
    
	const { codigo } = req.body;

    try {

		const existUser = await Usuario.findOne({codigo});

        if (!existUser) {
            return resp.status(404)
                .json({
                    ok: false,
                    msg: 'Usuario no encontrado en la BD!'
                });
		}
		
       await Usuario.findByIdAndDelete(existUser.id);
        return resp.status(200).json({
            ok: true,
            msg: "Usuario eliminado"
        });

    } catch (error) {
        return resp.status(500).json({
            ok: true,
            msg: 'Error inesperado.'
        });
    }
};


const updateUsuario = async(req, resp = response) => {
	
	//se excluye la propiedad id del objeto -> rest operator
	const { codigo, ...campos } = req.body;

    try {

		
		const existUser = await Usuario.findOne({codigo});

        if (!existUser) {
            return resp.status(404)
                .json({
                    ok: false,
                    msg: 'Usuario no encontrado en la BD!'
                });
		}
		
		// if(codigo !== existUser.codigo)
		// {
		// 	const existCode = await Usuario.findOne({ codigo });
        //     if (existCode) {
        //         return resp.status(400)
        //             .json({
        //                 ok: false,
        //                 msg: 'El código ya se encuentra registrado!'
        //             });
        //     }
		// }

	   campos.codigo = codigo;
       const usuario = await Usuario.findByIdAndUpdate(existUser.id,campos,{ new: true } );
        return resp.status(200).json({
            ok: true,
            msg: usuario
        });

    } catch (error) {
        return resp.status(500).json({
            ok: true,
            msg: 'Error inesperado.'
        });
    }
};

module.exports ={
	getUsuarios,
	crearUsuario,
	eliminarUsuario,
	updateUsuario
}