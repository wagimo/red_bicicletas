
var Bicicleta = require('../../models/bicicleta');
const { response } = require('express');



const getBicicletas = async(req, resp) => {

    
    const from = Number(req.query.from) || 0;
    const to = Number(req.query.to) || 5;

    const [bicicletas, total] = await Promise.all([
        Bicicleta.find({}, 'codigo color modelo ubicacion')
        .skip(from)
        .limit(to),
        Bicicleta.countDocuments()
    ]);
    
    resp.json({
        ok: true,
        bicicletas,        
        total
    });
};

const crearBicicleta = async (req, resp = response) => {
	
	const { latitud,  longitud } = {...req.body}

	try	{

		// let result = await Bicicleta.findOne({ codigo });

		// if (result) {
		// 	return resp.status(400)
		// 		.json({
		// 			ok: false,
		// 			msg: `El codigo ingresado: '${codigo}' ya se encuentra registrado en la BD! `
		// 		});
		// }		

		const bici = new Bicicleta( req.body );

		bici.ubicacion = [latitud, longitud];

		await bici.save();

		resp.json({
			ok: true,
			bici
		});


	} catch (err) {
        console.log(err);
        resp.status(500).json({
            ok: false,
            msg: 'Error inesperado. Revise la consola'
        });
    }
	

};

const eliminarBicicleta = async(req, resp = response) => {
    
	const { codigo } = req.body;

    try {

		const existBicicleta = await Bicicleta.findOne({codigo});

        if (!existBicicleta) {
            return resp.status(404)
                .json({
                    ok: false,
                    msg: 'Bicicleta no encontrada en la BD!'
                });
		}
		
       await Bicicleta.findByIdAndDelete(existBicicleta.id);
        return resp.status(200).json({
            ok: true,
            msg: "Bicicleta eliminada"
        });

    } catch (error) {
        return resp.status(500).json({
            ok: true,
            msg: 'Error inesperado.'
        });
    }
};


const updateBicicleta = async(req, resp = response) => {
	
	//se excluye la propiedad id del objeto -> rest operator
	const { id,codigo,...campos } = req.body;

    try {

		
		 const existBicicleta = await Bicicleta.findOne({codigo});

        if (!existBicicleta) {
            return resp.status(404)
                .json({
                    ok: false,
                    msg: 'Bicicleta no encontrada en la BD!'
                });
		}
		
		// if(codigo !== existBicicleta.codigo)
		// {
		// 	const existCode = await Bicicleta.findOne({ codigo });
        //     if (existCode) {
        //         return resp.status(400)
        //             .json({
        //                 ok: false,
        //                 msg: 'El código asignado a la bicicleta ya se encuentra registrado!'
        //             });
        //     }
		// }

	   campos.codigo = codigo;
       const bicileta = await Bicicleta.findByIdAndUpdate(existBicicleta.id,campos,{ new: true } );
        return resp.json({
            ok: true,
            msg: bicileta
        });

    } catch (error) {
        return resp.status(500).json({
            ok: true,
            msg: 'Error inesperado.'
        });
    }
};

module.exports ={
	getBicicletas,
	crearBicicleta,
	eliminarBicicleta,
	updateBicicleta
}

