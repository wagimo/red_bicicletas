const { response } = require("express");
var Usuario = require('../../models/usuario');
var Bicicleta = require('../../models/bicicleta');
var Reserva = require('../../models/reserva');
const reserva = require("../../models/reserva");


const getReservas = async(req, resp = response) => {
    try {

        const reservas = await Reserva.find().populate('usuario').populate('bicicleta');

        return resp.status(200).json({
            ok: true,
            reservas
        });
    } catch (error) {

        return resp.status(500).json({
            ok: true,
            msg: 'Error al retornar la lista de reservas!'
        });
    }
};


const crearReserva = async(req, resp = response) => {
    
    console.log("req.body",req.body);

    const reserva = new Reserva({    
		...req.body
    });

    try {

        const reservaBd = await reserva.save();
        return resp.json({
            ok: true,
            msg: reservaBd
        });

    } catch (error) {
        return resp.status(500).json({
            ok: true,
            msg: 'Error inesperado.' + error
        });
    }
};

const eliminarReserva = async(req, resp = response) => {
    
	const { id } = req.body;

    try {

		const existReserva = await Reserva.findById(id);

        if (!existReserva) {
            return resp.status(404)
                .json({
                    ok: false,
                    msg: 'Reserva no encontrada en la BD!'
                });
		}
		
       await reserva.findByIdAndDelete(id);
        return resp.status(204).json({
            ok: true,
            msg: "Reserva eliminada"
        });

    } catch (error) {
        return resp.status(500).json({
            ok: true,
            msg: 'Error inesperado.' + error
        });
    }
};


const updateReserva = async(req, resp = response) => {
	
	//se excluye la propiedad id del objeto -> rest operator
	const { codigo, ...campos } = req.body;

    try {

		
		const existReserva = await Reserva.findOne(codigo);

        if (!existReserva) {
            return resp.status(404)
                .json({
                    ok: false,
                    msg: 'Reserva no encontrada en la BD!'
                });
		}
		
		// if(codigo !== existReserva.codigo)
		// {
		// 	const existCode = await Reserva.findOne({ codigo });
        //     if (existCode) {
        //         return resp.status(400)
        //             .json({
        //                 ok: false,
        //                 msg: 'El código de reserva ya se encuentra registrado!'
        //             });
        //     }
		// }

	   campos.codigo = codigo;
       const reserva = await Reserva.findByIdAndUpdate(existReserva.id,campos,{ new: true } );
        return resp.status(200).json({
            ok: true,
            msg: reserva
        });

    } catch (error) {
        return resp.status(500).json({
            ok: true,
            msg: 'Error inesperado.'
        });
    }
};

module.exports ={
	getReservas,
	crearReserva,
	eliminarReserva,
	updateReserva
}