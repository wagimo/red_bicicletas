const Usuario = require('../../models/usuario');
const { response } = require('express');
const bcrypt = require('bcrypt');
const { generarToken } = require('../../helpers/jwt');


const loginusuario = async(req, resp = response) => {
	try
	{
		const {email, password} = req.body;
		if (email == null) {
            return resp.status(404)
                .json({
                    ok: false,
                    msg: 'Email o contraseña incorrecto!'
                });
		}
		
		const usuarioDb = await Usuario.findOne({ email });
        if (!usuarioDb) {
            return resp.status(404)
                .json({
                    ok: false,
                    msg: 'Email o contraseña incorrecto!'
                });
		}
		
		const validPassword = await bcrypt.compareSync(password, usuarioDb.password);
        if (!validPassword) {
            return resp.status(400)
                .json({
                    ok: false,
                    msg: 'Email o contraseña incorrecto!'
                });
        }

		const token = await generarToken(usuarioDb.id);
		resp.json({
            ok: true,
            token
        });

	}
	catch (err) {
        console.log(err);
        resp.status(500).json({
            ok: false,
            msg: 'Error inesperado. Revise la consola'
        });
    }
}

const renewToken = async(req, resp = response) => {

    try {

        const uid = req.uid;
        const token = await generarToken(uid);
        resp.json({
            ok: true,
            token
        });

    } catch (error) {
        console.log(error);
        return resp
            .status(500)
            .json({
                ok: false,
                msg: 'Error inesperado!'
            });
    }
};

const forgotPassword = (req, resp = response, next)=>{

    const email = req.body.email;
    if(!email)
    {
        return resp.status(500).json({
            ok: false,
            msg: 'Email incorrecto!'
        });
    }

    Usuario.findOne({email}, (err, user)=>{

        if(err)
        {
            return resp.status(500).json({
                ok: false,
                msg: err.error
            });
        }

        if (!user) {
            return resp.status(404)
                .json({
                    ok: false,
                    msg: 'Email o contraseña incorrecto!'
                });
		}
    });

    user.resetPassword(err=>{
        if(err)
        {
            return next(err);
        }
        return resp.status(200)
                .json({
                    ok: true,
                    msg: 'Se ha enviado un email con las instrucciones para la recuperacion del password!'
                });
    });

}

const authFacebookToken = async(req, resp = response, next) => {
    console.log("req.user-->>");
    if(req.user)
    {
        const token = await generarToken(req.user.id);
        console.log(token);
        req.user.save().then( ()=> {           
            resp.status(200).json({
                ok: true,
                token
            });
        }).catch( err =>{
            console.log(err);
            resp.status(500).json(
                {
                  ok:false,
                  message: err.message      
                }
            );
        });
    }
    else{
        resp.status(401);
    }
}


module.exports = {
    loginusuario,   
    renewToken,
    forgotPassword,
    authFacebookToken
};