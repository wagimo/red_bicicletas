
var Usuario = require('../models/usuario');



exports.usuarios_list = function(req, res)  {
	Usuario.find({}, (err, usuarios)=>{
		res.render('usuarios/index', {listado: usuarios});
	});
}


//metodo para ir inicialmente al formulario de creación
exports.usuarios_create_get = function(req, res)  {
	res.render('usuarios/create',{errors:{}, usuario: new Usuario()});
}

exports.usuarios_create_post = function(req, res)  {

	const {password, confirmpassword } = req.body;

	if(password !== confirmpassword)
	{
		res.render('usuarios/create', { 
				errors: {
					confirmpassword:{
						message:'El password no coincide!'
					} 
				},
				usuario: new Usuario({codigo:req.body.codigo, nombre: req.body.nombre, email: req.body.email })
			}
		);
		return;
	}

	var usuario = new Usuario( req.body  );	
	usuario.save(function(err, success){
		
		if(err) {
			console.log("Error al guardar el usuario en la BD!", err)
			res.render('usuarios/create', { 
				errors:err.errors, 
				usuario: new Usuario({codigo:req.body.codigo,nombre:req.body.nombre, email: req.body.email} ) 
			});
		}else{
			success.sendEmailActivacion();
			res.redirect('/usuarios');
		}
	});
}

exports.usuarios_update_get = function(req, res)  {
	
	Usuario.findById(req.params.id, (err, usuario) =>{
		
		if(err){
			next(err);
		}else{			
			res.render('usuarios/update',{errors:{},usuario});
		}
	});	
}

exports.usuarios_update_post = function(req, res)  {

	const id = req.params.id;
	const {email,password, ...campos } = req.body;
	

	Usuario.findByIdAndUpdate(id, campos,( err, usuario ) => {
		
		if(err){
			console.log(err);
			res.render('usuarios/update',{
				errors: err.errors,
				usuario: new Usuario(
					{
						codigo:req.body.codigo, 
						nombre: req.body.nombre
					}
				)
			});
		}else{
			res.redirect('/usuarios')
		}
	});	
}

exports.usuario_delete_post = function(req, res, next)  {
	
	Usuario.findByIdAndDelete(req.params.id, err => {

		if(err){
			next(err);
		}else{
			res.redirect('/usuarios');
		}
	});
	
}

exports.usuario_preview_get = function(req, res)  {
	
	const  id = req.params.id;	
	Usuario.findById(id, (err, usuario) => {
		if(usuario)
		{
			res.render('usuarios/preview',{usuario});
		}
	});
	
}