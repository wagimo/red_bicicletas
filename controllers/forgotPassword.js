

var Usuario = require('../models/usuario');
var Token = require('../models/token');

exports.forgotPassword = function(req, res)  {
	res.render('security/forgotPassword');
}

exports.forgotPasswordpost = function(req, res, next)  {

	const email = req.body.email;
	if(!email)
	{
		return res.render('security/forgotPassword',{info:{message:'Debe ingresar un correo electronico válido'}});
	}

	Usuario.findOne({email:email}, (err, user)=>{
		// if(err){
		// 	return res.render('security/forgotPassword',{info:{message:err}});			
		// }
		if(!user)
		{
			res.render('security/forgotPassword',{info:{message:'El correo electrónico no se encuentra registrado!!'}});
		}
		else
		{
			user.resetPassword((err)=>{
				if(err){								
					return next(err);
				}	
				else{
					res.render('security/forgotPasswordResult');
				}		
			});
		}
		
	});
}

exports.forgotPasswordResult = function(req, res){
	res.render('security/forgotPasswordResult');
}

exports.resetPasswordGet = function(req, res){

	const token = req.params.token;
	
	if(!token)
	{
		return res.render('security/resetPassword',{info:{message:'Error al leer el token desde la URL!'}});
	}

	Token.findOne({token}, (err, oToken)=>{	
		if(err)
		{
			return res.render('security/resetPassword',{info:{ message:err }});
		}

		if(!oToken) {
			return res.status(400)
			.send(
				{ 
					type: 'not-verified', 
					msg: 'No existe un usuario con el token asociado.'
				});	
		};
		
		Usuario.findById(oToken.usuario, (err, user) =>{
			if(err)
			{
				return res.render('security/resetPassword',{info:{ message:err }});
			}
			if(!user)
			{
				return res.render('security/resetPassword',{info:{ message:'No fue posible recuperar la información del usuario a través del token.' }});
			}
		
			res.render('security/resetPassword', {errors:{}, usuario: {email: user.email} });
		});
	});
}
	
	

exports.resetPasswordPost = function(req, res){
	
	const {password, confirmpassword, email } = req.body;

	if(password !== confirmpassword)
	{
		return res.render('security/resetPassword', {
			errors: {
				confirmpassword:{
					message:'El password no coincide!'
				} 
			}, usuario: new Usuario({ email: req.body.email })  
		});
	}
	
	Usuario.findOne({email}, (err, user)=>{

		if(err){
			return res.render('security/resetPassword',
			 { 
				info:{ message:'Error. usuario no encontrado en la BD!!'},
				usuario: new Usuario({email: email} ) 				
			 });
		}
		
		user.password = password;
		user.save(function(err, success){
		
			if(err) {					
				res.render('security/resetPassword', 
				{ 
					info:{ message:'Error al guardar el usuario en la BD!'},
					usuario: new Usuario({email: email} ) 
				});
			}else{				
				res.redirect('/login');
			}
		});
	});


	
	
}