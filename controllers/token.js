
var Token = require('../models/token');
var Usuario = require('../models/usuario');

exports.confirmarToken = function(req, res)  {
	const token = req.params.token;

	Token.findOne({token:token},(err, token) =>{
		if(!token) 
		{
			return res
			.status(400)
			.send({ type: 'not-verified', msg: 'Error al obtener el Token desde la BD!!'});
		}

		Usuario.findById(token.usuario, (err, user)=> {

			if(!user){
				return res
				.status(404)
				.send({ msg: 'Usuario no encontrado en la BD!'});
			}

			if(user.verificado) {
				return res.redirect('/usuarios');
			}

			user.verificado = true;
			user.save( err => {
				if(err)
				{
					return res
					.status(500)
					.send({ msg: err.message}); 
				}
				res.redirect('/');
			});

		});
	});	
}
