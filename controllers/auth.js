
var Usuario = require('../models/usuario');
const passport = require('../config/passport');
const { NotExtended } = require('http-errors');

//metodo para ir inicialmente al formulario de creación
exports.login = function(req, res)  {
	res.render('security/login');
}

exports.loginPost = function(req, res, next){
	passport.authenticate('local', function(error, user, info){
		if(error) return next(error);
		if(!user) return res.render("security/login", {info});
		req.login(user, function(err){
			if(err) return next (err);
			return res.redirect('/');
		});
	})(req, res, next);
}

exports.logout = function(req, res){
	req.logout();
	res.redirect('/login');
}