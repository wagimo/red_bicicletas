
var Bicicleta = require('../models/bicicleta');



exports.bicicleta_list = function(req, res)  {
	Bicicleta.find({}, (err, bicicletas)=>{
		res.render('bicicletas/index', {listado: bicicletas});
	});
}


//metodo para ir inicialmente al formulario de creación
exports.bicicleta_create_get = function(req, res)  {
	res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res)  {

	const {latitud, longitud, ...campos} = req.body;
	
	var bici = new Bicicleta( campos );
	bici.ubicacion = [latitud, longitud];

	bici.save((err, success)=>{
		if(err) {
			console.log("Error al guardar el usuario en la BD!", err)
			res.render('bicicletas/create', { errors: {error:err} });
		}else{
			res.redirect('/bicicletas');
		}
	});
}

exports.bicicleta_delete_post = function(req, res, next)  {
	
	Bicicleta.findByIdAndDelete(req.params.id, err =>{

		if(err){
			next(err);
		}else{
			res.redirect('/bicicletas');
		}
	});
	
}

exports.bicicleta_update_get = function(req, res)  {
	
	Bicicleta.findById(req.params.id, (err, bici) =>{
		if(err){
			next(err);
		}else{
			res.render('bicicletas/update',{bici});
		}
	});	
}

exports.bicicleta_update_post = function(req, res)  {

	const id = req.params.id;
	const { ...campos } = req.body;
	campos.ubicacion = [campos.latitud, campos.longitud];
		
	Bicicleta.findByIdAndUpdate(id, campos,( err, bici ) => {
		
		if(err){
			console.log(err);
			res.render('bicicletas/update',{
				errors: err.errors,
				bici: new Bicicleta(
					{
						codigo:req.body.codigo, 
						color: req.body.color, 
						modelo: req.body.modelo, 
						ubicacion:[req.body.latitud,req.body.longitud] 
					}
				)
			});
		}else{
			res.redirect('/bicicletas')
		}
	});	
}

exports.bicicleta_preview_get = function(req, res)  {
	
	const  id = req.params.id;
	console.log("id-->>", id);
	Bicicleta.findById(id, (err, bici) => {
		if(bici)
		{
			res.render('bicicletas/preview',{bici});
		}
	});
	
}